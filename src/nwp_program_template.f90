!------------------------------------------------------------------------------!
! Numerical Weather Prediction - Programming Excercise
!------------------------------------------------------------------------------!
! Numerische Wettervorhersage Praktikum Programm

! TODO: Add description
!  Add a proper description of program. Should include how to steer (including
!  required input) and what to expect as output.
!  2018-02-14, TG

!------------------------------------------------------------------------------!
! Author: Eva-Larissa Koch
! Date  : 2019-02-04 - 2019-02-08
!------------------------------------------------------------------------------!
! NOTE: Code compilation
!  When compiling the code, NetCDF libraries must be included/linked like so
!    ifort nwp_program.f90 -I <path-to-netcdf>/include -L <path-to-netcdf>/lib -lnetcdff
!  2018-02-07, TG

PROGRAM nwp_program

!  USE netcdf

   IMPLICIT NONE


!- Declare variables
   CHARACTER(LEN=100) :: file_out = "nwp_output"   !< name of output file (namelist param)

   INTEGER :: i               !< loop index
   INTEGER :: j               !< loop index
   INTEGER :: k               !< loop index
   INTEGER :: t               !< loop index
   INTEGER :: k_max = 1000    !< maximum iteration count of SOR method (namelist param)
   INTEGER :: nx = 1000       !< number of grid points along x (namelist param)
   INTEGER :: ny = 1000       !< number of grid points along y (namelist param)
   INTEGER :: nt              !< number of time steps
   INTEGER :: AllocateStatus  !< AllocateStatus
   INTEGER :: text_case       !< number of case wanted

   REAL :: beta               !< beta parameter
   REAL :: delta = 10000.0    !< grid width (namelist param)
   REAL :: dt                 !< time-step width
   REAL :: dt_do              !< timestep of data output
   REAL :: f_0                !< Coriolis parameter at y=0
   REAL :: latitude = 52.0    !< latitude (namelist param)
   REAL :: omega              !< relaxation factor of SOR method
   REAL :: t_end              !< time to simulate (s)
   REAL :: t_sim              !< simulated time (s)
   REAL :: time_end = 24.0    !< time to simulate (h, namelist param)
   REAL :: time_dt_do = 24.0  !< time interval of output (h, namelist param)
   REAL :: u_bg               !< background wind speed
   REAL :: u_max              !< maximum wind speed

   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolis parameter along y

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: u       !< wind speed along x
   REAL, DIMENSION(:,:), ALLOCATABLE :: v       !< wind speed along y
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< vorticity at time t
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_m  !< vorticity at time t-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_p  !< vorticity at time t+1

   REAL, DIMENSION(:,:,:), ALLOCATABLE :: phi_ref  !< reference geopotential

   REAL, PARAMETER :: g = 9.81      !< acceleration of gravity
   REAL, PARAMETER :: pi = 3.14159  !< pi


!- Define and read init-namelist
   namelist / INIT / delta, file_out, k_max, latitude, nx, ny, time_dt_do,     &
                     time_end, text_case

!- Start
   WRITE(*, '(A//A)') ' --- NWP program, V0.0.1 ---',  &
                      ' --- start forecast...'

  OPEN(1, FILE='./nwp_namelist.init', STATUS='OLD', &
                                      FORM='FORMATTED', IOSTAT=ioerr)
  IF (ioerr /= 0) THEN
       CALL error_message('errNLO')
  ENDIF

  READ(1, NML=INIT, IOSTAT=ioerr)
  IF (ioerr /= 0) THEN
       CALL error_message('errNLR')
  ENDIF

  CLOSE(1)


!- Initialize variables
! Convert from hours into seconds
   t_end = time_end * 3600.0 ![t_end]=s
   !...

! wind speed setup
   u_max = 10.0      ![umax]=m/s
   !...

! Coriolis parameter setup
   f_0 = 2.*pi/(86400.)*SIN(latitude/180.*pi)

! In x-Richtung +-1 Feld für 'ghost layer'.
   ALLOCATE(f(0:ny), STAT = AllocateStatus)
   ALLOCATE(phi(-1:nx+1,0:ny), STAT = AllocateStatus)
!    ALLOCATE(u(-1:nx+1, ... ), STAT = AllocateStatus)
!    ALLOCATE(v(0:ny, ... ), STAT = AllocateStatus)
   ALLOCATE(zeta(-1:nx+1,0:ny), STAT = AllocateStatus)
   ALLOCATE(zeta_m(-1:nx+1,0:ny), STAT = AllocateStatus)
   ALLOCATE(zeta_p(-1:nx+1,0:ny), STAT = AllocateStatus)
   ALLOCATE(phi_ref(-1:nx+1,0:ny,-1:1), STAT = AllocateStatus)	! (i,j,k)
   IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"

CALL SOR(phi)


SELECT CASE (text_case)
  CASE (1)

    zeta = 0
    phi = 0
    
! Randwerte
    phi(0,:) = 0
    phi(ny,:) = 50

    DO
      phi = (phi(ny,i)/(ny*delta))*j
    ENDDO

  CASE (2)

    zeta = 10**(-4)
    phi = 0

    DO
      phi = (1/2)*f*zeta*(j**2)+(phi(ny,i)/ny)-(1/2)*f*zeta*ny*j
      phi(ny,i)=0
    ENDDO

  CASE (3)

    phi_ref = 0
    phi = 0
    phi_a = u_max*(f*ny)/pi
    zeta = -1/f * ((4*pi)/((nx)**2) + ((pi**2)/((ny)**2)))
    u_max = 10
    
    DO
      phi_ref=u_max*(f*ny)/pi* SIN(((2*pi)/ny)*i)* SIN((pi)/ny)*j)
    ENDDO



!
!    !- Read input data
!    CALL read_input_data(...)
!
!
!    !- Open output file
!    CALL data_output('open', file_out)
!
!
!    !- Start forecast
!    t_sim = 0.0
!    nt = 0
!    DO...
!
!       CALL get_vorticity(...)
!
!       CALL get_geopot(...)
!
!       CALL get_wind(...)
!
!       CALL data_output(...)
!
!    ENDDO
!
!
!    !- Finalize output
!    CALL data_output(...)
!

   !- End
   WRITE(*, '(A)') ' --- Finished! :-) ---'


CONTAINS
!- Functions and subroutines
!
!    REAL FUNCTION timestep()
!    ! Calculate timestep
!      ...
!    END FUNCTION timestep

!----------------------------------------------------------------------------

SUBROUTINE SOR(phi)
! näherungsweise Lösung der Poisson-Gleichung

!- Declare variables
   INTEGER :: i               !< loop index
   INTEGER :: j               !< loop index
   INTEGER :: k               !< loop index
   INTEGER :: nx = 399        !< number of grid points along x (namelist param)
   INTEGER :: ny = 200        !< number of grid points along y (namelist param)

   REAL :: delta = 30000.0    !< grid width (namelist param)
   REAL :: f_0                !< Coriolis parameter at y=0
   REAL :: omega              !< relaxation factor of SOR method
   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolis parameter along y

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< vorticity at time t

   REAL, PARAMETER :: g = 9.81                  !< acceleration of gravity
   REAL, PARAMETER :: pi = 3.14159              !< pi
   REAL, DIMENSION(:,:), ALLOCATABLE  :: epsi	  !< epsilon
   REAL :: cola					                        !< max(epsi)
   REAL :: pepsi = 0.001		                    !< Grenzwert (von epsi)

   ALLOCATE(epsi(-1:nx+1,0:ny))

   ALLOCATE(f(0:ny), STAT = AllocateStatus)
   ALLOCATE(phi(-1:nx+1,0:ny), STAT = AllocateStatus)
   ALLOCATE(zeta(-1:nx+1,0:ny), STAT = AllocateStatus)

! Konvergenzfaktor (zwischen 0 und 2)
   omega = 2. - 2.*pi/(SQRT(2.)) * SQRT((nx+1.)**(-2.) + (ny +1.)**(-2.))

! Startwerte
   phi = 0
   zeta = 0
   f_0 = 10**(-4)
   f = f_0
   cola = 2. * pepsi
   k = 0

   DO WHILE (cola > pepsi)
   k = k+1
! Phi im Modelbereich
    DO i = 0, nx
      DO j = 1, ny-1
	       epsi(j,i) = phi(j,i)

	       phi(j,i) = phi(j,i)*(1-omega) + (omega/4) * &
                    (phi(j,i+1)+phi(j,i-1)+phi(j+1,i)+phi(j-1,i) &
			              -(f(j)*(delta**(2))*zeta(j,i)))

	       epsi(j,i) = ABS(phi(j,i) - epsi(j,i))
      END DO
    END DO

! ghost layer
    DO j = 0, ny
      phi(nx+1, j) = phi(0, j)
      phi(-1, j) = phi(nx, j)
    END DO

! Abbruchkriterium vorbereiten
   cola = MAXVAL(epsi)

   END DO

   WRITE(*,*) k

END SUBROUTINE SOR

!----------------------------------------------------------------------------
!
!    SUBROUTINE error_message(...)
  !    ! Print error message and close program
!       ....
!    END SUBROUTINE error_message
!
!    !----------------------------------------------------------------------------
!
!    SUBROUTINE get_geopot(max_it)
!    ! Calculate geopotential via SOR method
!
!       INTEGER, INTENT (IN) :: max_it  !< maximum iteration count
!
!       REAL :: ...
!       ...
!    END SUBROUTINE get_geopot
!
!    !----------------------------------------------------------------------------
!
!    SUBROUTINE get_vorticity(...)
!    ! Calculate vorticity via prognostic equation
!       ....
!    END SUBROUTINE get_vorticity
!
!    !----------------------------------------------------------------------------
!
!    SUBROUTINE get_wind(...)
!    ! Calculate wind speed components via geostrophic wind relation
!       ...
!    END SUBROUTINE get_wind
!
!    !----------------------------------------------------------------------------
!
!    SUBROUTINE read_input_data(...)
!    ! Read input from mesoscale model
!       ...
!    SUBROUTINE read_input_data
!
!    !----------------------------------------------------------------------------
!
!    SUBROUTINE data_output(action, output)
!    ! Data output to NetCDF file
!
!       CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine (open/close/write)
!       CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix
!
!       INTEGER, SAVE :: do_count=0      !< counting output
!       INTEGER, SAVE :: id_dim_time     !< ID of dimension time
!       INTEGER, SAVE :: id_dim_x        !< ID of dimension x
!       INTEGER, SAVE :: id_dim_y        !< ID of dimension time
!       INTEGER, SAVE :: id_file         !< ID of NetCDF file
!       INTEGER, SAVE :: id_var_phi      !< ID of geopotential
!       INTEGER, SAVE :: id_var_phiref   !< ID of reference geopotential
!       INTEGER, SAVE :: id_var_time     !< ID of time
!       INTEGER, SAVE :: id_var_u        !< ID of wind speed along x
!       INTEGER, SAVE :: id_var_v        !< ID of wind speed along y
!       INTEGER, SAVE :: id_var_x        !< ID of x
!       INTEGER, SAVE :: id_var_y        !< ID of y
!       INTEGER, SAVE :: id_var_zeta     !< ID of vorticity
!       INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines
!
!       INTEGER :: tt                    !< time index of ref. geopot.
!
!       REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array
!
!       REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array
!
!
!       SELECT CASE (TRIM(action))
!
!          !- Initialize output file
!          CASE ('open')
!
!             !- Delete any pre-existing output file
!             OPEN(20, FILE=TRIM(output)//'.nc')
!             CLOSE(20, STATUS='DELETE')
!
!             !- Open file
!             nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
!             IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'
!
!             !- Write global attributes
!             nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
!                                     'Conventions', 'COARDS')
!             nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
!                                     'title', 'barotropic nwp-model')
!
!             !- Define time coordinate
!             nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED,  &
!                                     id_dim_time)
!             nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE,  &
!                                     id_dim_time, id_var_time)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units',  &
!                                     'seconds since 1900-1-1 00:00:00')
!
!             !- Define spatial coordinates
!             nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
!             nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
!                                     id_dim_x, id_var_x)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')
!
!             nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
!             nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
!                                     id_dim_y, id_var_y)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')
!
!             !- Define output arrays
!             nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE,           &
!                                     (/id_dim_x, id_dim_y, id_dim_time/),  &
!                                     id_var_phi)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
!                                     'long_name', 'geopotential at 500hPa')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
!                                     'short_name', 'geopotential')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')
!
!             nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE,       &
!                                     (/id_dim_x, id_dim_y, id_dim_time/),  &
!                                     id_var_phiref)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
!                                     'long_name',            &
!                                     'reference geopotential at 500hPa')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
!                                     'short_name', 'ref. geopotential')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')
!
!             nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE,          &
!                                     (/id_dim_x, id_dim_y, id_dim_time/),  &
!                                     id_var_zeta)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
!                                     'long_name', 'vorticity at 500hPa')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
!                                     'short_name', 'vorticity')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')
!
!             nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE,             &
!                                     (/id_dim_x, id_dim_y, id_dim_time/),  &
!                                     id_var_u)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_u,  &
!                                     'long_name',       &
!                                     'u component of wind')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')
!
!             nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE,             &
!                                     (/id_dim_x, id_dim_y, id_dim_time/),  &
!                                     id_var_v)
!             nc_stat = NF90_PUT_ATT(id_file, id_var_v,  &
!                                     'long_name',       &
!                                     'v component of wind')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
!             nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')
!
!
!             !- Leave define mode
!             nc_stat = NF90_ENDDEF(id_file)
!
!             !- Write axis to file
!             ALLOCATE(netcdf_data_1d(0:nx))
!
!             !- x axis
!             DO  i = 0, nx
!                netcdf_data_1d(i) = i * delta
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
!                                     start = (/1/), count = (/nx+1/))
!             !- y axis
!             DO  j = 0, ny
!                netcdf_data_1d(j) = j * delta
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
!                                     start = (/1/), count = (/ny+1/))
!
!             DEALLOCATE(netcdf_data_1d)
!
!
!          !- Close NetCDF file
!          CASE ('close')
!
!             nc_stat = NF90_CLOSE(id_file)
!
!
!          !- Write data arrays to file
!          CASE ('write')
!
!             ALLOCATE(netcdf_data_2d(0:nx,0:ny))
!
!             do_count = do_count + 1
!
!             !- Write time
!             nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/),  &
!                                     start = (/do_count/),               &
!                                     count = (/1/))
!
!             !- Write geopotential
!             DO  i = 0, nx
!                DO  j = 0, ny
!                   netcdf_data_2d(i,j) = phi(j,i)
!                ENDDO
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d,  &
!                                     start = (/1, 1, do_count/),          &
!                                     count = (/nx+1, ny+1, 1/))
!
!             !- Write reference geopotential
!             tt =...     ! time index of reference geopotential
!             DO  i = 0, nx
!                DO  j = 0, ny
!                   netcdf_data_2d(i,j) = phi_ref(j,i,tt)
!                ENDDO
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d,  &
!                                     start = (/1, 1, do_count/),             &
!                                     count = (/nx+1, ny+1, 1/))
!
!             !- Write vorticity
!             DO  i = 0, nx
!                DO  j = 0, ny
!                   netcdf_data_2d(i,j) = zeta(j,i)
!                ENDDO
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d,  &
!                                     start = (/1, 1, do_count/),           &
!                                     count = (/nx+1, ny+1, 1/))
!
!             !- Write u
!             DO  i = 0, nx
!                DO  j = 0, ny
!                   netcdf_data_2d(i,j) = u(j,i)
!                ENDDO
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d,  &
!                                     start = (/1, 1, do_count/),        &
!                                     count = (/nx+1, ny+1, 1/))
!
!             !- Write v
!             DO  i = 0, nx
!                DO  j = 0, ny
!                   netcdf_data_2d(i,j) = v(j,i)
!                ENDDO
!             ENDDO
!             nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d,  &
!                                     start = (/1, 1, do_count/),        &
!                                     count = (/nx+1, ny+1, 1/))
!
!             DEALLOCATE(netcdf_data_2d)
!
!
!          !- Print error message if unknown action selected
!          CASE DEFAULT
!
!             WRITE(*, '(A)') ' ** data_output: action "'//  &
!                               TRIM(action)//'"unknown!'
!
!       END SELECT
!
!    END SUBROUTINE data_output


END PROGRAM nwp_program
